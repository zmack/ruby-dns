# encoding: ASCII-8BIT
# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/dns'
# Elsewhere after Bundler has loaded gems e.g. after `require 'bundler/setup'`
require 'minitest/unit'
require 'mocha/mini_test'

describe 'DNS::Packer' do
  describe 'pack_flags' do
    it 'raises when given nil' do
      assert_raises do
        DNS::Packer.pack_flags(nil)
      end
    end
    describe 'packing' do
      values = [false, true]
      symbols = %i[qr aa tc rd]
      values.repeated_permutation(symbols.size).each do |v|
        m = symbols.zip(v)
        flags = Hash[m]
        describe "with #{flags}" do
          before do
            @flags = flags
            @packed = DNS::Packer.pack_flags(@flags)
          end

          it 'packs qr correctly' do
            bit = (@packed & 128) != 0
            assert_equal(@flags[:qr], bit)
          end

          it 'packs aa correctly' do
            bit = (@packed & 4) != 0
            assert_equal(@flags[:aa], bit)
          end

          it 'packs tc correctly' do
            bit = (@packed & 2) != 0
            assert_equal(@flags[:tc], bit)
          end

          it 'packs rd correctly' do
            bit = (@packed & 1) != 0
            assert_equal(@flags[:rd], bit)
          end
        end
      end
    end
  end

  describe 'pack_ra_z_rcode' do
    before do
      @header = {
        flags: {},
        z: 0,
        rcode: 0
      }
    end
    describe 'with ra flag set' do
      before do
        @header[:flags][:ra] = true
      end

      it 'should be greater than or equal to 128' do
        actual = DNS::Packer.pack_ra_z_rcode(@header)
        assert_operator(actual, :>=, 128)
      end
    end

    describe 'with ra flag unset' do
      before do
        @header[:flags][:ra] = false
      end

      it 'should be less than 128' do
        actual = DNS::Packer.pack_ra_z_rcode(@header)
        assert_operator(actual, :<, 128)
      end
    end

    describe 'with non zero z values' do
      before do
        @header[:z] = 1
      end
      it 'should raise ' do
        assert_raises do
          DNS::Packer.pack_ra_z_rcode(@header)
        end
      end
    end

    describe 'with legal code values' do
      (0..16).each do |rcode|
        describe "with rcode value #{rcode}" do
          before do
            @header[:rcode] = rcode
          end

          it "with rcode value #{rcode} it should pack the rcode in the bottom bits" do
            actual = DNS::Packer.pack_ra_z_rcode(@header)
            assert_equal(rcode, actual)
          end
        end
      end
    end
    describe 'with illegal code values' do
      (17..32).each do |rcode|
        describe "with rcode value #{rcode}" do
          before do
            @header[:rcode] = rcode
          end

          it "with rcode #{rcode} it should raise" do
            assert_raises do
              DNS::Packer.pack_ra_z_rcode(@header)
            end
          end
        end
      end
    end
  end

  describe 'pack_header' do
    it 'should pack a zero header' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 0,
        qd_count: 0,
        an_count: 0,
        ns_count: 0,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", data)
    end

    it 'should pack a header with an rcode' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 15,
        qd_count: 0,
        an_count: 0,
        ns_count: 0,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x0F\x00\x00\x00\x00\x00\x00\x00\x00", data)
    end

    it 'should pack a header with ra and rcode' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: true,
          opcode: 0
        },
        z: 0,
        rcode: 15,
        qd_count: 0,
        an_count: 0,
        ns_count: 0,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x8F\x00\x00\x00\x00\x00\x00\x00\x00", data)
    end

    it 'should pack a header with id' do
      @header = {
        id: 127,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 0,
        qd_count: 0,
        an_count: 0,
        ns_count: 0,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x7F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", data)
    end

    it 'should pack a header with qd count' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 0,
        qd_count: 26,
        an_count: 0,
        ns_count: 0,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x00\x00\x1A\x00\x00\x00\x00\x00\x00", data)
    end

    it 'should pack a header with an count' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 0,
        qd_count: 0,
        an_count: 26,
        ns_count: 0,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x00\x00\x00\x00\x1A\x00\x00\x00\x00", data)
    end

    it 'should pack a header with ns count' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 0,
        qd_count: 0,
        an_count: 0,
        ns_count: 26,
        ar_count: 0
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1A\x00\x00", data)
    end

    it 'should pack a header with ar count' do
      @header = {
        id: 0,
        flags: {
          qr: false,
          aa: false,
          tc: false,
          rd: false,
          ra: false,
          opcode: 0
        },
        z: 0,
        rcode: 0,
        qd_count: 0,
        an_count: 0,
        ns_count: 0,
        ar_count: 26
      }
      data = DNS::Packer.pack_header(@header)
      assert_equal("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1A", data)
    end
  end

  describe 'pack_qds' do
    it 'packs empty qds' do
      assert_empty(DNS::Packer.pack_qds([]))
    end

    it 'packs a single empty qd' do
      assert_equal("\x00\x00\x00\x00\x00", DNS::Packer.pack_qds([{ tags: [], type: 0, klass: 0 }]))
    end

    it 'packs a single qd with tags' do
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x00\x00\x00", DNS::Packer.pack_qds([{ tags: %w[www stspg io], type: 0, klass: 0 }]))
    end

    it 'packs a single qd with a type' do
      assert_equal("\x00\x00\x01\x00\x00", DNS::Packer.pack_qds([{ tags: [], type: 1, klass: 0 }]))
    end

    it 'packs a single qd with a klass' do
      assert_equal("\x00\x00\x00\x00\x01", DNS::Packer.pack_qds([{ tags: [], type: 0, klass: 1 }]))
    end

    it 'packs multiple qds' do
      qds = [
        { tags: [], type: 0, klass: 0 },
        { tags: %w[www stspg io], type: 0, klass: 0 },
        { tags: [], type: 1, klass: 0 },
        { tags: [], type: 0, klass: 1 }
      ]
      expected = "\x00\x00\x00\x00\x00\x03www\x05stspg\x02io\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x01"
      assert_equal(expected, DNS::Packer.pack_qds(qds))
    end
  end
  describe 'pack_ans' do
    it 'packs empty ans' do
      assert_empty(DNS::Packer.pack_ans([]))
    end

    it 'packs a single A an' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(1)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(address: '152.33.25.11')

      assert_equal("\x03www\x05stspg\x02io\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x04\x98!\x19\v", DNS::Packer.pack_ans([rr_record]))
    end

    it 'packs a single ns an' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(2)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(rsdname: %w[www stspg com])

      assert_equal("\x03www\x05stspg\x02io\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x0F\x03www\x05stspg\x03com\x00", DNS::Packer.pack_ans([rr_record]))
    end

    it 'packs a single cname an' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(5)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(cname: 'www.stspg.com')
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x0F\x03www\x05stspg\x03com\x00", DNS::Packer.pack_ans([rr_record]))
    end

    it 'packs a single soa an' do
      skip 'Not implemented ANDREI!'
    end

    it 'packs a single mx an' do
      skip 'Not implemented ANDREI!'
    end

    it 'packs a single txt an' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(16)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(txt_data: 'lol wut')
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\b\alol wut", DNS::Packer.pack_ans([rr_record]))
    end
  end

  describe 'pack_nss' do
    it 'packs empty nss' do
      assert_empty(DNS::Packer.pack_nss([]))
    end

    it 'packs a single A ns' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(1)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(address: '152.33.25.11')

      assert_equal("\x03www\x05stspg\x02io\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x04\x98!\x19\v", DNS::Packer.pack_nss([rr_record]))
    end

    it 'packs a single ns ns' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(2)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(rsdname: %w[www stspg com])

      assert_equal("\x03www\x05stspg\x02io\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x0F\x03www\x05stspg\x03com\x00", DNS::Packer.pack_nss([rr_record]))
    end

    it 'packs a single cname ns' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(5)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(cname: 'www.stspg.com')
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x0F\x03www\x05stspg\x03com\x00", DNS::Packer.pack_nss([rr_record]))
    end

    it 'packs a single soa ns' do
      skip 'Not implemented ANDREI!'
    end

    it 'packs a single mx ns' do
      skip 'Not implemented ANDREI!'
    end

    it 'packs a single txt ns' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(16)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(txt_data: 'lol wut')
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\b\alol wut", DNS::Packer.pack_nss([rr_record]))
    end
  end

  describe 'pack_ars' do
    it 'packs empty ars' do
      assert_empty(DNS::Packer.pack_ars([]))
    end

    it 'packs a single A ar' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(1)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(address: '152.33.25.11')

      assert_equal("\x03www\x05stspg\x02io\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x04\x98!\x19\v", DNS::Packer.pack_ars([rr_record]))
    end

    it 'packs a single ar ns' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(2)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(rsdname: %w[www stspg com])

      assert_equal("\x03www\x05stspg\x02io\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x0F\x03www\x05stspg\x03com\x00", DNS::Packer.pack_ars([rr_record]))
    end

    it 'packs a single cname ar' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(5)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(cname: 'www.stspg.com')
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x0F\x03www\x05stspg\x03com\x00", DNS::Packer.pack_ars([rr_record]))
    end

    it 'packs a single soa ar' do
      skip 'Not implemented ANDREI!'
    end

    it 'packs a single mx ar' do
      skip 'Not implemented ANDREI!'
    end

    it 'packs a single txt ar' do
      rr_record = mock
      rr_record.stubs(:name).returns('www.stspg.io')
      rr_record.stubs(:type).returns(16)
      rr_record.stubs(:klass).returns(0)
      rr_record.stubs(:ttl).returns(0)
      rr_record.stubs(:rdata).returns(txt_data: 'lol wut')
      assert_equal("\x03www\x05stspg\x02io\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\b\alol wut", DNS::Packer.pack_ars([rr_record]))
    end
  end
end

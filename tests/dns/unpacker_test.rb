# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/dns'

FALSY_VALUES = [false, 0, nil].freeze
def false?(obj)
  FALSY_VALUES.include?(obj)
end

describe 'DNS::Unpacker' do
  before do
    @cara_cname = %w[ 57 b3 85 80 00 01 00 01 00 00 00 00 06 73 74 61
                      74 75 73 04 63 61 72 61 03 63 6f 6d 00 00 05 00
                      01 c0 0c 00 05 00 01 00 00 0e 0a 00 21 0c 67 6c
                      78 77 74 76 31 7a 34 6d 37 39 0e 73 74 73 70 67
                      2d 63 75 73 74 6f 6d 65 72 03 63 6f 6d 00].map { |x| x.to_i(16).chr }.join.freeze

    @txt_ok = %w[ 77 f4 81 80 00 01 00 02 00 00 00 00 03 61 77 73
                  03 63 6f 6d 00 00 10 00 01 c0 0c 00 10 00 01 00
                  00 01 2c 00 0e 0d 4d 53 3d 6d 73 39 33 36 36 38
                  38 31 37 c0 0c 00 10 00 01 00 00 01 2c 00 2f 2e
                  76 3d 73 70 66 31 20 69 6e 63 6c 75 64 65 3a 73
                  70 66 2e 70 72 6f 74 65 63 74 69 6f 6e 2e 6f 75
                  74 6c 6f 6f 6b 2e 63 6f 6d 20 7e 61 6c 6c].map { |x| x.to_i(16).chr }.join.freeze

    @mx_ok =  %w[ 09 ea 81 80 00 01 00 01 00 00 00 00 03 61 77 73
                  03 63 6f 6d 00 00 0f 00 01 c0 0c 00 0f 00 01 00
                  00 01 2c 00 24 00 00 07 61 77 73 2d 63 6f 6d 04
                  6d 61 69 6c 0a 70 72 6f 74 65 63 74 69 6f 6e 07
                  6f 75 74 6c 6f 6f 6b c0 10].map { |x| x.to_i(16).chr }.join.freeze

    @ns_ok = %w[ 33 da 81 80 00 01 00 04 00 00 00 00 03 61 77 73
                 03 63 6f 6d 00 00 02 00 01 c0 0c 00 02 00 01 00
                 01 46 9a 00 19 07 6e 73 2d 31 38 31 38 09 61 77
                 73 64 6e 73 2d 33 35 02 63 6f 02 75 6b 00 c0 0c
                 00 02 00 01 00 01 46 9a 00 13 06 6e 73 2d 31 33
                 39 09 61 77 73 64 6e 73 2d 31 37 c0 10 c0 0c 00
                 02 00 01 00 01 46 9a 00 17 07 6e 73 2d 31 34 39
                 30 09 61 77 73 64 6e 73 2d 35 38 03 6f 72 67 00
                 c0 0c 00 02 00 01 00 01 46 9a 00 16 06 6e 73 2d
                 35 32 37 09 61 77 73 64 6e 73 2d 30 31 03 6e 65
                 74 00].map { |x| x.to_i(16).chr }.join.freeze

    @a_record = "\xcf\x63\x81\x80\x00\x01\x00\x01\x00\x00\x00\x00\x09\x75\x73\x2d\x77\x65\x73\x74\x2d\x32\x07\x63\x6f\x6e\x73\x6f\x6c\x65\x03\x61\x77\x73\x06\x61\x6d\x61\x7a\x6f\x6e\x03\x63\x6f\x6d\x00\x00\x01\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x3c\x00\x04\x36\xf0\xfe\xf6"
  end
  after do
  end

  describe 'unpack' do
    it 'raises on empty' do
      assert_raises do
        DNS::Unpacker.unpack(StringIO.new(''))
      end
    end

    it 'unpacks cnames' do
      message = DNS::Unpacker.unpack(StringIO.new(@cara_cname))
      message.header.wont_be_nil
      message.qd.wont_be_nil
      message.an.wont_be_nil
      message.ns.wont_be_nil
      message.ar.wont_be_nil

      message.header[:id].must_equal 22_451
      message.header[:z].must_equal 0
      message.header[:rcode].must_equal 0
      message.header[:qd_count].must_equal 1
      message.header[:an_count].must_equal 1
      message.header[:ns_count].must_equal 0
      message.header[:ar_count].must_equal 0

      FALSY_VALUES.wont_include message.header[:flags][:qr]
      message.header[:flags][:opcode].must_equal 0
      FALSY_VALUES.wont_include message.header[:flags][:aa]
      FALSY_VALUES.must_include message.header[:flags][:tc]
      FALSY_VALUES.wont_include message.header[:flags][:rd]
      FALSY_VALUES.wont_include message.header[:flags][:ra]

      message.qd.must_equal [{ tags: %w[status cara com], type: 5, klass: 1 }]
      message.an.size.must_equal 1
      message.an.first.name.must_equal 'status.cara.com'
      message.an.first.type.must_equal 5
      message.an.first.klass.must_equal 1
      message.an.first.ttl.must_equal 3594
      message.an.first.rdata.wont_be_nil
      message.an.first.rdata[:cname].must_equal 'glxwtv1z4m79.stspg-customer.com'
      message.ns.must_be_empty
      message.ar.must_be_empty
    end

    it 'unpacks txt' do
      message = DNS::Unpacker.unpack(StringIO.new(@txt_ok))
      message.wont_be_nil
      message.header.wont_be_nil
      message.qd.wont_be_nil
      message.an.wont_be_nil
      message.ns.wont_be_nil
      message.ar.wont_be_nil

      message.header[:id].must_equal 30_708
      message.header[:z].must_equal 0
      message.header[:rcode].must_equal 0
      message.header[:qd_count].must_equal 1
      message.header[:an_count].must_equal 2
      message.header[:ns_count].must_equal 0
      message.header[:ar_count].must_equal 0

      FALSY_VALUES.wont_include message.header[:flags][:qr]
      message.header[:flags][:opcode].must_equal 0
      FALSY_VALUES.must_include message.header[:flags][:aa]
      FALSY_VALUES.must_include message.header[:flags][:tc]
      FALSY_VALUES.wont_include message.header[:flags][:rd]
      FALSY_VALUES.wont_include message.header[:flags][:ra]

      message.qd.must_equal [{ tags: %w[aws com], type: 16, klass: 1 }]
      message.an.size.must_equal 2

      message.an.first.name.must_equal 'aws.com'
      message.an.first.type.must_equal 16
      message.an.first.klass.must_equal 1
      message.an.first.ttl.must_equal 300
      message.an.first.rdata.wont_be_nil
      message.an.first.rdata[:txt_data].must_equal 'MS=ms93668817'

      message.an[1].name.must_equal 'aws.com'
      message.an[1].type.must_equal 16
      message.an[1].klass.must_equal 1
      message.an[1].ttl.must_equal 300
      message.an[1].rdata.wont_be_nil
      message.an[1].rdata[:txt_data].must_equal 'v=spf1 include:spf.protection.outlook.com ~all'

      message.ns.must_be_empty
      message.ar.must_be_empty
    end

    it 'unpacks mx' do
      message = DNS::Unpacker.unpack(StringIO.new(@mx_ok))
      message.header.wont_be_nil
      message.qd.wont_be_nil
      message.an.wont_be_nil
      message.ns.wont_be_nil
      message.ar.wont_be_nil

      message.header[:id].must_equal 2538
      message.header[:z].must_equal 0
      message.header[:rcode].must_equal 0
      message.header[:qd_count].must_equal 1
      message.header[:an_count].must_equal 1
      message.header[:ns_count].must_equal 0
      message.header[:ar_count].must_equal 0

      FALSY_VALUES.wont_include message.header[:flags][:qr]
      message.header[:flags][:opcode].must_equal 0
      FALSY_VALUES.must_include message.header[:flags][:aa]
      FALSY_VALUES.must_include message.header[:flags][:tc]
      FALSY_VALUES.wont_include message.header[:flags][:rd]
      FALSY_VALUES.wont_include message.header[:flags][:ra]

      message.qd.must_equal [{ tags: %w[aws com], type: 15, klass: 1 }]
      message.an.size.must_equal 1
      message.an.first.name.must_equal 'aws.com'
      message.an.first.type.must_equal 15
      message.an.first.klass.must_equal 1
      message.an.first.ttl.must_equal 300
      message.an.first.rdata.wont_be_nil
      message.an.first.rdata[:preference].must_equal 0
      message.an.first.rdata[:exchange].must_equal 'aws-com.mail'
      message.ns.must_be_empty
      message.ar.must_be_empty
    end

    it 'unpacks ns' do
      message = DNS::Unpacker.unpack(StringIO.new(@ns_ok))
      message.header.wont_be_nil
      message.qd.wont_be_nil
      message.an.wont_be_nil
      message.ns.wont_be_nil
      message.ar.wont_be_nil

      message.header[:id].must_equal 13_274
      message.header[:z].must_equal 0
      message.header[:rcode].must_equal 0
      message.header[:qd_count].must_equal 1
      message.header[:an_count].must_equal 4
      message.header[:ns_count].must_equal 0
      message.header[:ar_count].must_equal 0

      FALSY_VALUES.wont_include message.header[:flags][:qr]
      message.header[:flags][:opcode].must_equal 0
      FALSY_VALUES.must_include message.header[:flags][:aa]
      FALSY_VALUES.must_include message.header[:flags][:tc]
      FALSY_VALUES.wont_include message.header[:flags][:rd]
      FALSY_VALUES.wont_include message.header[:flags][:ra]

      message.qd.must_equal [{ tags: %w[aws com], type: 2, klass: 1 }]
      message.an.size.must_equal 4

      message.an[0].name.must_equal 'aws.com'
      message.an[0].type.must_equal 2
      message.an[0].klass.must_equal 1
      message.an[0].ttl.must_equal 83_610
      message.an[0].rdata.wont_be_nil
      message.an[0].rdata[:nsdname].must_equal 'ns-1818.awsdns-35.co.uk'

      message.an[1].name.must_equal 'aws.com'
      message.an[1].type.must_equal 2
      message.an[1].klass.must_equal 1
      message.an[1].ttl.must_equal 83_610
      message.an[1].rdata.wont_be_nil
      message.an[1].rdata[:nsdname].must_equal 'ns-139.awsdns-17'

      message.an[2].name.must_equal 'aws.com'
      message.an[2].type.must_equal 2
      message.an[2].klass.must_equal 1
      message.an[2].ttl.must_equal 83_610
      message.an[2].rdata.wont_be_nil
      message.an[2].rdata[:nsdname].must_equal 'ns-1490.awsdns-58.org'

      message.an[3].name.must_equal 'aws.com'
      message.an[3].type.must_equal 2
      message.an[3].klass.must_equal 1
      message.an[3].ttl.must_equal 83_610
      message.an[3].rdata.wont_be_nil
      message.an[3].rdata[:nsdname].must_equal 'ns-527.awsdns-01.net'

      message.ns.must_be_empty
      message.ar.must_be_empty
    end

    it 'unpacks A' do
      message = DNS::Unpacker.unpack(StringIO.new(@a_record))
      message.header.wont_be_nil
      message.qd.wont_be_nil
      message.an.wont_be_nil
      message.ns.wont_be_nil
      message.ar.wont_be_nil

      message.header[:id].must_equal 53_091
      message.header[:z].must_equal 0
      message.header[:rcode].must_equal 0
      message.header[:qd_count].must_equal 1
      message.header[:an_count].must_equal 1
      message.header[:ns_count].must_equal 0
      message.header[:ar_count].must_equal 0

      FALSY_VALUES.wont_include message.header[:flags][:qr]
      message.header[:flags][:opcode].must_equal 0
      FALSY_VALUES.must_include message.header[:flags][:aa]
      FALSY_VALUES.must_include message.header[:flags][:tc]
      FALSY_VALUES.wont_include message.header[:flags][:rd]
      FALSY_VALUES.wont_include message.header[:flags][:ra]

      message.qd.must_equal [{ tags: %w[us-west-2 console aws amazon com], type: 1, klass: 1 }]
      message.an.size.must_equal 1
      message.an.first.name.must_equal 'us-west-2.console.aws.amazon.com'
      message.an.first.type.must_equal 1
      message.an.first.klass.must_equal 1
      message.an.first.ttl.must_equal 60
      message.an.first.rdata.wont_be_nil
      message.an.first.rdata[:address].must_equal '54.240.254.246'
      message.ns.must_be_empty
      message.ar.must_be_empty
    end
  end

  describe 'parse_header' do
    it 'raises on empty' do
      assert_raises do
        DNS::Unpacker.parse_header(StringIO.new(''))
      end
    end
  end

  describe 'parse_type' do
    it 'raises on empty' do
      assert_raises do
        DNS::Unpacker.parse_type(StringIO.new(''))
      end
    end
  end

  describe 'parse_tags' do
    it 'raises on empty' do
      assert_raises do
        DNS::Unpacker.parse_tags(StringIO.new(''))
      end
    end
  end
end

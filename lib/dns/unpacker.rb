# frozen_string_literal: true

module DNS
  # Unpacks dns thingos
  module Unpacker
    class << self
      def cache_tags(offset, tag)
        @cache ||= {}
        @cache[offset] = tag
      end

      def get_cached_tags(offset)
        @cache ||= {}
        @cache[offset]
      end

      def unpack(payload)
        header = parse_header(payload)
        message = Message.new(header)

        message.qd = parse_qds(header[:qd_count], payload)
        message.an = parse_ans(header[:an_count], payload)
        message.ns = parse_nss(header[:ns_count], payload)
        message.ar = parse_ars(header[:ar_count], payload)

        message
      end

      def parse_header(payload)
        id = payload.gets(2).unpack1('S>')
        flags = payload.getc.unpack1('C')
        ra_z_rcode = payload.getc.unpack1('C')

        qr = flags & 128
        opcode = (flags - qr) >> 3
        aa = flags & 4
        tc = flags & 2
        rd = flags & 1
        ra = flags & 128

        z = (ra_z_rcode - ra) >> 4
        rcode = ra_z_rcode & 15

        qd_count = payload.gets(2).unpack1('S>')
        an_count = payload.gets(2).unpack1('S>')
        ns_count = payload.gets(2).unpack1('S>')
        ar_count = payload.gets(2).unpack1('S>')

        {
          id: id,
          flags: {
            qr: qr,
            opcode: opcode,
            aa: aa,
            tc: tc,
            rd: rd,
            ra: ra
          },
          z: z,
          rcode: rcode,
          qd_count: qd_count,
          an_count: an_count,
          ns_count: ns_count,
          ar_count: ar_count
        }
      end

      def parse_type(payload)
        type, klass = payload.gets(4).unpack('S>2')
        [type, klass]
      end

      def parse_qd(payload)
        tags = parse_tags(payload)
        type_data, klass = parse_type(payload)
        { tags: tags, type: type_data, klass: klass }
      end

      def parse_qds(count, payload)
        Array.new(count) do
          parse_qd(payload)
        end
      end

      def parse_ans(count, payload)
        Array.new(count) do
          parse_rr(payload)
        end
      end

      def parse_nss(count, payload)
        Array.new(count) do
          parse_rr(payload)
        end
      end

      def parse_ars(count, payload)
        Array.new(count) do
          parse_rr(payload)
        end
      end

      def parse_rr(payload)
        name = parse_rr_name(payload)
        type = payload.gets(2).unpack1('S>')
        klass = payload.gets(2).unpack1('S>')
        ttl = payload.gets(4).unpack1('L>')

        rdata = parse_length_prefixed_short(payload)
        parsed_rdata = rr_parse_record(type, StringIO.new(rdata))

        RRRecord.new(
          name: name,
          klass: klass,
          type: type,
          ttl: ttl,
          rdata: parsed_rdata
        )
      end

      def parse_rr_name(payload)
        parse_tags(payload).join('.')
      end

      def parse_length_prefixed_short(payload)
        length = payload.gets(2).unpack1('S>')

        payload.gets(length)
      end

      def parse_length_prefixed(payload)
        length = payload.getc&.unpack1('C')
        if length > 63
          offset = ((length & 63) << 8) + payload.getc.unpack1('C')
          return get_cached_tags(offset)
        end

        payload.gets(length)
      end

      def parse_tags(payload, skip_cache: false)
        current_offset = payload.pos unless skip_cache
        tags = []

        loop do
          tag = parse_length_prefixed(payload)
          break if tag.nil? || tag.empty?

          if tag.is_a? Array
            tags.concat(tag)
            break
          else
            tags << tag
          end
        end

        cache_tags(current_offset, tags) unless skip_cache

        tags
      end

      def rr_parse_record(type, rdata)
        case type
        when 1
          rr_parse_record_a(rdata)
        when 2
          rr_parse_record_ns(rdata)
        when 5
          rr_parse_record_cname(rdata)
        when 6
          rr_parse_record_soa(rdata)
        when 15
          rr_parse_record_mx(rdata)
        when 16
          rr_parse_record_txt(rdata)
        when 41
          rr_parse_record_opt(rdata)
        else
          raise "Unknown type: #{type} #{rdata.inspect}"
        end
      end

      def rr_parse_record_opt(rdata)
        {
          opt: rdata.read
        }
      end

      def rr_parse_record_a(rdata)
        {
          address: rdata.read.unpack('C4').map(&:to_s).join('.')
        }
      end

      def rr_parse_record_ns(rdata)
        {
          nsdname: parse_tags(rdata, skip_cache: true).join('.')
        }
      end

      def rr_parse_record_mx(rdata)
        {
          preference: rdata.gets(2).unpack1('S>'),
          exchange: parse_tags(rdata, skip_cache: true).join('.')
        }
      end

      def rr_parse_record_cname(rdata)
        {
          cname: parse_tags(rdata, skip_cache: true).join('.')
        }
      end

      def rr_parse_record_soa(rdata)
        {
          mname: parse_tags(rdata, skip_cache: true).join('.'),
          rname: parse_tags(rdata, skip_cache: true).join('.'),
          serial: rdata.gets(4).unpack1('L>'),
          refresh: rdata.gets(4).unpack1('L>'),
          retry: rdata.gets(4).unpack1('L>'),
          expire: rdata.gets(4).unpack1('L>'),
          minimum: rdata.gets(4).unpack1('L>')
        }
      end

      def rr_parse_record_txt(rdata)
        {
          txt_data: parse_length_prefixed(rdata)
        }
      end
    end
  end
end

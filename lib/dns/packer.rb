# frozen_string_literal: true

module DNS
  # Unpacks dns thingos
  module Packer
    class << self
      def pack(message)
        buffer = pack_header(message.header)
        buffer << pack_qds(message.qd)
        buffer << pack_ans(message.an)
        buffer << pack_nss(message.ns)
        buffer << pack_ars(message.ar)
      end

      def pack_flags(flags)
        packed_flags = false?(flags[:qr]) ? 0 : 128
        packed_flags += flags[:opcode].to_i << 4
        packed_flags += false?(flags[:aa]) ? 0 : 4
        packed_flags += false?(flags[:tc]) ? 0 : 2
        packed_flags += false?(flags[:rd]) ? 0 : 1
        packed_flags
      end

      def pack_ra_z_rcode(header)
        packed_bits = false?(header[:flags][:ra]) ? 0 : 128
        packed_bits += header[:z] << 3
        packed_bits += header[:rcode]
        packed_bits
      end

      def pack_header(header)
        data = [
          header[:id],
          pack_flags(header[:flags]),
          pack_ra_z_rcode(header),
          header[:qd_count],
          header[:an_count],
          header[:ns_count],
          header[:ar_count]
        ]

        data.pack('S>CCS>4')
      end

      def pack_qd(qd)
        buffer = pack_tags(qd[:tags])
        buffer << [qd[:type], qd[:klass]].pack('S>2')
      end

      def pack_qds(qds)
        qds.map(&method(:pack_qd)).join
      end

      def pack_ans(rr_records)
        rr_records.map(&method(:pack_rr)).join
      end

      def pack_nss(rr_records)
        rr_records.map(&method(:pack_rr)).join
      end

      def pack_ars(rr_records)
        rr_records.map(&method(:pack_rr)).join
      end

      def pack_rr(rr_record)
        buffer = pack_rr_name(rr_record)
        buffer << [
          rr_record.type, rr_record.klass,
          rr_record.ttl
        ].pack('S>S>L>')

        buffer << pack_length_prefixed_short(rr_pack_record(rr_record.type, rr_record.rdata))
      end

      def pack_rr_name(rr_record)
        pack_tags(rr_record.name.split('.'))
      end

      def pack_length_prefixed_short(string)
        [string.length].pack('S>') + string
      end

      def pack_length_prefixed(string)
        [string.length].pack('C') + string
      end

      def pack_tags(tags)
        tags.map do |tag|
          pack_length_prefixed(tag)
        end.join('') << "\0"
      end

      def rr_pack_record(type, rdata)
        case type
        when 1
          rr_pack_record_a(rdata)
        when 2
          rr_pack_record_ns(rdata)
        when 5
          rr_pack_record_cname(rdata)
        when 6
          rr_pack_record_soa(rdata)
        when 15
          rr_pack_record_mx(rdata)
        when 16
          rr_pack_record_txt(rdata)
        when 41
          rr_pack_record_opt(rdata)
        else
          raise "Unknown type: #{type} #{rdata.inspect}"
        end
      end

      def rr_pack_record_opt(rdata)
        rdata
      end

      def rr_pack_record_a(rdata)
        rdata[:address].split('.').map(&:to_i).pack('C4')
      end

      def rr_pack_record_ns(rdata)
        pack_tags(rdata[:rsdname])
      end

      def rr_pack_record_mx(rdata)
        {
          preference: rdata.gets(2).unpack1('S>'),
          exchange: pack_tags(rdata).join('.')
        }
      end

      def rr_pack_record_cname(rdata)
        pack_tags(rdata[:cname].split('.'))
      end

      def rr_pack_record_soa(rdata)
        {
          mname: pack_tags(rdata, skip_cache: true).join('.'),
          rname: pack_tags(rdata, skip_cache: true).join('.'),
          serial: rdata.gets(4).unpack1('L>'),
          refresh: rdata.gets(4).unpack1('L>'),
          retry: rdata.gets(4).unpack1('L>'),
          expire: rdata.gets(4).unpack1('L>'),
          minimum: rdata.gets(4).unpack1('L>')
        }
      end

      def rr_pack_record_txt(rdata)
        pack_length_prefixed(rdata[:txt_data])
      end

      private

      FALSY_VALUES = [false, 0, nil].freeze
      def false?(obj)
        FALSY_VALUES.include?(obj)
      end
    end
  end
end

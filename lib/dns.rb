# frozen_string_literal: true

require_relative './dns/unpacker.rb'
require_relative './dns/packer.rb'

module DNS
  class Message
    attr_accessor :header, :qd, :an, :ns, :ar

    def initialize(header = nil)
      @header = header
      @qd = []
      @an = []
      @ns = []
      @ar = []
    end
  end

  class Response < Message
    def initialize(query)
      header = {
        id: query.header[:id],
        flags: {
          qr: 1,
          opcode: query.header[:opcode] || 0,
          aa: 1,
          tc: 0,
          rd: 0,
          ra: 0
        },
        z: 0
      }

      super(header)
      @qd = query.qd
    end

    def header
      @header.merge(
        qd_count: qd.length,
        an_count: an.length,
        ns_count: ns.length,
        ar_count: ar.length,
        rcode: 0
      )
    end

    def add_cname_rr(name:, domain:, ttl:)
      record = RRRecord.new(
        name: name,
        rdata: { cname: domain },
        ttl: ttl,
        type: 5,
        klass: 1
      )
      @an << record
    end

    def add_a_rr(name:, address:, ttl:)
      record = RRRecord.new(
        name: name,
        rdata: { address: address },
        ttl: ttl,
        type: 1,
        klass: 1
      )
      @an << record
    end

    def add_txt_rr(name:, data:, ttl:)
      record = RRRecord.new(
        name: name,
        rdata: data,
        ttl: ttl,
        type: 16,
        klass: 1
      )
      @an << record
    end
  end

  class RRRecord
    attr_accessor :name, :type, :klass, :ttl, :rdata

    def initialize(parsed_data)
      @name = parsed_data[:name]
      @type = parsed_data[:type]
      @klass = parsed_data[:klass]
      @ttl = parsed_data[:ttl]
      @rdata = parsed_data[:rdata]
    end
  end
end
